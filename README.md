# README #

R Custom model in Sagemaker

## Examples ##

* [Example 1](https://github.com/awslabs/amazon-sagemaker-examples/tree/master/advanced_functionality/r_bring_your_own)
* [Example 2 (HPO)](https://github.com/awslabs/amazon-sagemaker-examples/tree/master/r_examples/r_byo_r_algo_hpo)
* [Example 3 (Python)](https://github.com/awslabs/amazon-sagemaker-examples/tree/master/advanced_functionality/scikit_bring_your_own)

## Overview ##

### container folder ###

#### Contains files necessary to build container ####

### data ###

* ```test.csv``` – data to fit LM1
* ```test2.csv``` – data to fit LM2

### Notebooks ###

* ```creat_image.ipynb``` - Build docker container & push to Amazon ECR
* ```fit-lm1.ipynb``` – Fit first model
* ```fit-lm2.ipynb``` - Fit second model

### Lambda function ###

#### Input examples ####

##### Single prediction #####

```json
{
  "Age": "19.95",
  "Gender": "2",
  "Previous Grade": "31.01"
}
```

##### Multiple predictions 1 #####

```json
{
  "Age": [
    "18.25975",
    "19.46446"
  ],
  "Gender": [
    "2",
    "2"
  ],
  "Previous Grade": [
    "79.72007",
    "75.8544"
  ]
}
```

##### Multiple predictions 2 #####

```json
[
  {
    "Age": "18.25975",
    "Gender": "2",
    "Previous.Grade": "79.72007"
  },
  {
    "Age": "19.46446",
    "Gender": "2",
    "Previous.Grade": "75.8544"
  }
]
```

#### Output ####

```json
{
  "statusCode": 200,
  "Input": {
    "Age": "19.95",
    "Gender": "2",
    "Previous Grade": "31.01"
  },
  "Predicted": [
    "34.3158027829883"
  ]
}
```
