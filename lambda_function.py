import json
import csv
import os
import io
import boto3

# Grab endpoint name (remember to set it in lambda UI)
ENDPOINT_NAME = os.environ['ENDPOINT_NAME']
runtime= boto3.client('runtime.sagemaker')


def lambda_handler(event, context):
    
    if isinstance(event, dict):
        
        colnames = ','.join(list(event.keys()))
        out_x = dict(event)
        
        if isinstance(list(event.values())[0], list):
            n_input = len(list(event.values())[0])
            vals = "\n".join([','.join([x[i] for x in event.values()]) for i in range(n_input)])
            
        else:
            n_input = 1
            vals = ','.join([x for x in event.values()])
        
    else:
        colnames = ', '.join(list(event[0].keys()))
        out_x = list(event)
        n_input = len(list(event))
        vals = "\n".join([",".join(list(x.values())) for x in event])
        
    x_in = colnames + "\n" + vals

    response = runtime.invoke_endpoint(EndpointName=ENDPOINT_NAME,
                                      ContentType='text/csv',
                                      Body=x_in)
    score = json.loads(response['Body'].read().decode())[0].split(",")
    
    return {
        'statusCode': 200, 
        'Input': out_x,
        'Predicted': score
    }
